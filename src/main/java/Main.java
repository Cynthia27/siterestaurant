import myproject.daos.UtilisateurDao;
import project.models.Utilisateur;

public class Main {

	public static void main(String[] args) {
		UtilisateurDao ud = new UtilisateurDao();
		
		Utilisateur u = ud.findById(1);
		u.getRoles().add("Admin");
		u.getRoles().remove("Utilisateur");
		ud.save(u);
	}

}

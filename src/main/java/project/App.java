package project;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;



public class App {

	public static void main( String[] args ) throws Exception{

	
		SessionFactory sf = new Configuration().configure().buildSessionFactory(); 
		Session s = sf.openSession();
		s.close();
		sf.close();
	}
}

package project.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="ligneproduit")
public class LigneProduit {
	
	@Id
	@GeneratedValue
	private int id;
	private int nb;
	@ManyToOne
	private Panier panier;
	@ManyToOne
	private Produits produits;
	
	
	public double prixTotal() {
		return nb * produits.getPrix();
	}
	
	public int totalArticles() {
		return nb * produits.getNb();
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	public void setNb(int nb) {
		this.nb = nb;
	}
	public void setPanier(Panier panier) {
		this.panier = panier;
	}
	public void setProduits(Produits produits) {
		this.produits = produits;
	}
	public int getId() {
		return id;
	}
	public int getNb() {
		return nb;
	}
	public Panier getPanier() {
		return panier;
	}
	public Produits getProduits() {
		return produits;
	}

}

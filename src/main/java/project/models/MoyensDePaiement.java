package project.models;

public enum MoyensDePaiement {
	
	CARTE_BANCAIRE,
	CARTE_TICKET_RESTAURANT,
	PAYPAL,

}

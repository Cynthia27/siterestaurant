package project.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="personne")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personne {
	
	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private String prenom;
	private String numeroDeTelephone;
	@OneToMany(mappedBy="personne")
	private List<Reservation> reservation;
	@ElementCollection(fetch =FetchType.EAGER)
	private Set<String> roles = new HashSet<String>();

	
	public Set<String> getRoles() {
		return roles;
	}


	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}


	public Personne() {
		
	}
	

	public Personne( String nom, String prenom, String numeroDeTelephone) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numeroDeTelephone = numeroDeTelephone;
	}
	
	public Personne( int id, String nom, String prenom, String numeroDeTelephone) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.numeroDeTelephone = numeroDeTelephone;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public List<Reservation> getReservation() {
		return reservation;
	}


	public void setReservation(List<Reservation> reservation) {
		this.reservation = reservation;
	}


	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNumeroDeTelephone() {
		return numeroDeTelephone;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setNumeroDeTelephone(String numeroDeTelephone) {
		this.numeroDeTelephone = numeroDeTelephone;
	}
	
	

}

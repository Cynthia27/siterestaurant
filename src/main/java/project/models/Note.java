package project.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name="note")
public class Note {
	
	@Id
	@GeneratedValue
	private int id;
	private double note;
	@ManyToOne
	private Produits produit;
	@OneToOne(mappedBy="note")
	private Utilisateur utilisateur;
	
	public Note() {
		
	}

	public double getNote() {
		return note;
	}

	public void setNote(double note) {
		this.note = note;
	}

	public int getId() {
		return id;
	}

	public Produits getProduit() {
		return produit;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setProduit(Produits produit) {
		this.produit = produit;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	

}

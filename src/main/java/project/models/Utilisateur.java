package project.models;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table (name="utilisateur")
public class Utilisateur extends Personne{

	
	@Override
	public String toString() {
		return "Utilisateur [password=" + password 
				+ ", email=" + email + ", adresse=" + adresse + ", moyensdePaiement=" + moyensdePaiement
				+ ", commandeUtilisateur=" + commandeUtilisateur + ", panier=" + panier + ", listeProduits="
				+ listeProduits + ", note=" + note + "]";
	}


	
	private String password;
	
	private String email;
	private String adresse;
	@Enumerated(EnumType.STRING)
	private MoyensDePaiement moyensdePaiement;
	
	@OneToMany(mappedBy="utilisateur",fetch=FetchType.EAGER)
	private Set<Commande> commandeUtilisateur;
	@OneToOne(cascade=CascadeType.ALL)
	private Panier panier;
	@OneToMany(mappedBy="utilisateur",fetch=FetchType.EAGER)
	private Set<Produits> listeProduits;
	@OneToOne
	private Note note;
	
	

	public Utilisateur() {
		this.panier = new Panier();
		this.getRoles().add("Utilisateur");
	}

	public Utilisateur( String nom, String prenom, String numeroDeTelephone, String password, String email, String adresse) {
		super( nom, prenom, numeroDeTelephone);
		this.panier = new Panier();
		this.password = password;
		this.email = email;
		this.adresse = adresse;
		this.getRoles().add("Utilisateur");
		
	}
	
	public Utilisateur(int id, String nom, String prenom, String numeroDeTelephone, String password, String email, String adresse) {
		super( id ,nom, prenom, numeroDeTelephone);
		this.panier = new Panier();
		this.password = password;
		this.email = email;
		this.adresse = adresse;
		this.getRoles().add("Utilisateur");
		
	}
	

	public Panier getPanier() {
		return panier;
	}


	


	public void setCommandeUtilisateur(List<Commande> commandeUtilisateur) {
		this.commandeUtilisateur = (Set<Commande>) commandeUtilisateur;
	}




	public void setPanierUtilisateur(Panier panierUtilisateur) {
		this.panier = panierUtilisateur;
	}




	public void setListeProduits(List<Produits> listeProduits) {
		this.listeProduits = (Set<Produits>) listeProduits;
	}




	public void setNote(Note note) {
		this.note = note;
	}




	public Panier getPanierUtilisateur() {
		return panier;
	}




	public Set<Produits> getListeProduits() {
		return  listeProduits;
	}




	public Note getNote() {
		return note;
	}




	

	public Set<Commande> getCommandeUtilisateur() {
		return commandeUtilisateur;
	}


	


	public String getPassword() {
		return password;
	}


	


	public String getEmail() {
		return email;
	}


	public String getAdresse() {
		return adresse;
	}


	public MoyensDePaiement getMoyensdePaiement() {
		return moyensdePaiement;
	}


	

	public void setPassword(String password) {
		this.password = password;
	}


	


	public void setEmail(String email) {
		this.email = email;
	}


	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public void setMoyensdePaiement(MoyensDePaiement moyensdePaiement) {
		this.moyensdePaiement = moyensdePaiement;
	}



}

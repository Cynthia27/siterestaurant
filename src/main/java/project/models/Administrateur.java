package project.models;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name="administrateur")
public class Administrateur extends Personne {
	
	private String email;
	private String password;

	public Administrateur() {
		super();
		this.getRoles().add("Admin");		
	}
	
	



	public Administrateur(String nom, String prenom, String numeroDeTelephone) {
		super(nom, prenom, numeroDeTelephone);
		this.email =email;
		this.password = password;
		this.getRoles().add("Admin");
	}





	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword (String password) {
		this.password = password;
	}
	
	
		

}

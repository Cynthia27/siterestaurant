package project.models;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table (name="reservation")
public class Reservation {

	@Id
	@GeneratedValue
	private int id;
	private String heureDArrivee;
	private String nombreDePersonnes;
	private LocalDate dateArrivee;
	@ManyToOne
	@XmlTransient
	private Personne personne;
	
	public Reservation(String heureDArrivee, String nombreDePersonnes,LocalDate dateArrivee,String nom, String prenom, String numeroDeTelephone) {
		super();
		this.heureDArrivee = heureDArrivee;
		this.nombreDePersonnes = nombreDePersonnes;
		this.dateArrivee = dateArrivee;
		
		
	}

	public Reservation() {
	}
	
	public void setDateArrivee(LocalDate dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public LocalDate getDateArrivee() {
		return dateArrivee;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	public Personne getPersonne() {
		return personne;
	}

	public int getId() {
		return id;
	}
	

	public void setId(int id) {
		this.id = id;
	}



	public String getHeureDArrivee() {
		return heureDArrivee;
	}

	public String getNombreDePersonnes() {
		return nombreDePersonnes;
	}

	public void setHeureDArrivee(String heureDArrivee) {
		this.heureDArrivee = heureDArrivee;
	}

	public void setNombreDePersonnes(String nombreDePersonnes) {
		this.nombreDePersonnes = nombreDePersonnes;
	}


}

package project.models;

import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;





@Entity
@Table (name="panier")
public class Panier {
	
	@Id
	@GeneratedValue
	private int id;
	
	private double totalCommande;
	private int quantiteLigneArticle;	
	private ZonedDateTime choixHeureDeLivraison;
	@OneToOne (mappedBy ="panier")
	private Utilisateur utilisateur;
	@ManyToMany(mappedBy="panier", fetch=FetchType.EAGER)
	private List<LigneProduit> ligneProduits;
	
	
	public int getId() {
		return id;
	}

	public List<LigneProduit> getLigneProduits() {
		return ligneProduits;
	}

	public MoyensDePaiement getMoyenDePaiement() {
		return moyenDePaiement;
	}

	@Enumerated(EnumType.STRING)
	private MoyensDePaiement moyenDePaiement;
	
	public Panier() {
		
	}
	
	public Panier(double totalCommande, int quantiteLigneArticle, 
			List<LigneProduit> ligneProduits) {
		super();
		this.totalCommande = totalCommande;
		this.quantiteLigneArticle = quantiteLigneArticle;
		this.ligneProduits = ligneProduits;
	}



	public double getTotalCommande() {
		return totalCommande;
	}

	public int getQuantiteLigneArticle() {
		return quantiteLigneArticle;
	}

	

	

	public ZonedDateTime getChoixHeureDeLivraison() {
		return choixHeureDeLivraison;
	}

	public void setTotalCommande(double totalCommande) {
		this.totalCommande = totalCommande;
	}

	public void setQuantiteLigneArticle(int quantiteLigneArticle) {
		this.quantiteLigneArticle = quantiteLigneArticle;
	}

	

	

	public void setChoixHeureDeLivraison(ZonedDateTime choixHeureDeLivraison) {
		this.choixHeureDeLivraison = choixHeureDeLivraison;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public List<LigneProduit> getListeProduits() {
		return ligneProduits;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public void setListeProduits(List<LigneProduit> ligneProduits) {
		this.ligneProduits = ligneProduits;
	}
	
	public int getNbProduits() {
		int nb =0;
		for (LigneProduit p : this.ligneProduits) {
			nb += p.getNb();
		}
		return nb;
	}
	
	public int getQuantite(Produits p) {
		for (LigneProduit lp: ligneProduits) {
			if (lp.getProduits().getId() == p.getId())
				return lp.getNb();
		}
		return 0;	
	}
	
	public double prixTotal() {
		double total = 0;
		for (LigneProduit lp : this.ligneProduits) 
			total += lp.prixTotal();
		return total;
	}
	
	public double totalArticles() {
		int total = 0;
		for (LigneProduit lp : this.ligneProduits) 
			total += lp.totalArticles();
		return total;
	}
	
	

}

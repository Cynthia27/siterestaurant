package project.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table (name="commande")
public class Commande {
	
	@Id
	@GeneratedValue
	private int id;
	@Enumerated(EnumType.STRING)
	private StatutCommande statutCommande;
	private int numeroDeCommande;	
	@OneToMany
	private List<LigneProduit> listeProd;
	@ManyToOne
	private Utilisateur utilisateur;
	
	public Commande() {
	}
	
	public Commande( 
			Utilisateur utilisateur,List<LigneProduit> listeProd) {
		super();
		this.listeProd = listeProd;
		this.utilisateur = utilisateur;
	}


	public int getId() {
		return id;
	}
 
	public void setId(int id) {
		this.id = id;
	}
	
	

	public List<LigneProduit> getListeProd() {
		return listeProd;
	}
	

	public void setListeProd(List<LigneProduit> listeProd) {
		this.listeProd = listeProd;
	}

	public StatutCommande getStatutCommande() {
		return statutCommande;
	}

	public int getNumeroDeCommande() {
		return numeroDeCommande;
	}

	

	public void setStatutCommande(StatutCommande statutCommande) {
		this.statutCommande = statutCommande;
	}

	public void setNumeroDeCommande(int numeroDeCommande) {
		this.numeroDeCommande = numeroDeCommande;
	}

	

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	
}

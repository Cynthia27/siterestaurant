package project.models;

public enum StatutCommande {
	
	VALIDEE,
	EN_COURS_DE_PREPARATION,
	EN_COURS_DE_LIVRAISON,
	LIVREE

}

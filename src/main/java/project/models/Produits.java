package project.models;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="produits")
public class Produits {

	

	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private Double prix;
	private String description;
	@ManyToMany
	private List<Commande> commande;
	private int nb;
	@ManyToMany
	private List<LigneProduit> ligneProduit;
	@ManyToOne
	private Utilisateur utilisateur;
	@ElementCollection (fetch=FetchType.EAGER)
	private Set<File> images = new HashSet<File>();
	@OneToMany
	private List<Note> note;
	
	

	public Produits() {
	}

	
	public Produits( String nom,Double prix, String description ) {
		super();
		this.nom =nom;
		this.prix = prix;
		this.description = description;
		
		
	}
	
	public int getNb() {
		return nb;
	}
	
	public void setNb(int nb) {
		this.nb = nb;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	

	public String getNom() {
		return nom;
	}


	public void setImages(Set<File> images) {
		this.images = images;
	}


	public Set<File> getImages() {
		return images;
	}


	public void setId(int id) {
		this.id = id;
	}

	

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public void setNote(List<Note> note) {
		this.note = note;
	}

	public void setCommande(List<Commande> commande) {
		this.commande = commande;
	}

	
	public int getId() {
		return id;
	}

	

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public List<Note> getNote() {
		return note;
	}

	public List<Commande> getCommande() {
		return commande;
	}



	public Double getPrix() {
		return prix;
	}

	public List<LigneProduit> getLigneProduit() {
		return ligneProduit;
	}


	public void setLigneProduit(List<LigneProduit> ligneProduit) {
		this.ligneProduit = ligneProduit;
	}


	public String getDescription() {
		return description;
	}

	

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	@Override
	public String toString() {
		return "Produits [id=" + id + ", nom=" + nom + ", prix=" + prix + ", description=" + description + ", nb=" + nb
				+ ", utilisateur=" + utilisateur + ", note=" + note + ", commande=" + commande
				+ "]";
	}


}

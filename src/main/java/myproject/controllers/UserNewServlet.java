package myproject.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myproject.services.PersonneService;
import project.models.Utilisateur;

@WebServlet("/user/signup")
public class UserNewServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	private PersonneService ps = new PersonneService();	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("../WEB-INF/jsp/userSignup.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Utilisateur newU = new 	Utilisateur(
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("numeroDeTelephone"),
				request.getParameter("password"),
				request.getParameter("email"),
				request.getParameter("adresse"));
	
ps.ajouterUtilisateur(newU);

		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		
	}
	
}






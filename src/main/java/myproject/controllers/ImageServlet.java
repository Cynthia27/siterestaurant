package myproject.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/img/*")
public class ImageServlet extends HttpServlet {

 
	

	  @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	    	
	    	String fileName = request.getPathInfo().substring(1);
	    	//System.out.println(ConfigurationUtil.getProperty("images.path"));
	    	InputStream is = new FileInputStream(new File("C:/Users/Admin/Documents/RestaurantPics/" + fileName));	
	    	OutputStream os = response.getOutputStream();
	    	is.transferTo(os);
	    	
	    }
	}
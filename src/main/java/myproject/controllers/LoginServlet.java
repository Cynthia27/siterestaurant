package myproject.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import myproject.services.PersonneService;
import project.models.Personne;

@WebServlet("/login")
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	private PersonneService us = new PersonneService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Personne p = null;
		try {
			p = us.signIn(request.getParameter("email"),
					request.getParameter("password"));
		} catch(PersistenceException e) {
			request.setAttribute("errorMessage", "Unable to connect to database. Please try again later..");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		}
		System.out.println("User: " + p);
		if (p == null) {
			request.setAttribute("errorMessage", "Email or password invalid.");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("user", p);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

}

package myproject.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myproject.services.PersonneService;
import project.models.Personne;
import project.models.Utilisateur;


@WebServlet("/user/edit")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PersonneService ps = new PersonneService();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur p = (Utilisateur) request.getSession().getAttribute("user");
		
		request.setAttribute("editedUser", p);
		request.getRequestDispatcher("../WEB-INF/jsp/userAccount.jsp").forward(request, response);
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		Personne oldC = (Personne) request.getSession().getAttribute("user");	
		Utilisateur newU = new Utilisateur(
				oldC.getId(),
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("numeroDeTelephone"),		
				request.getParameter("password"),
				request.getParameter("email"),
				request.getParameter("adresse"));
		//(request.getParameter("newPassword").isEmpty()) ? oldC.getPassword() : request.getParameter("newPassword"));
				
	
		
		// validation
//		if ((!request.getParameter("newPassword").isEmpty()) &&
//				(!request.getParameter("oldPassword").equals(oldC.getPassword()))) {
//			error = true;
//			request.setAttribute("passwordErrorMessage", "Wrong password");
//		}
		// mise a jour de la base de donnée
		try {
			ps.editerClient(oldC, newU);
		} catch(PersistenceException e) {
			error = true;
			request.setAttribute("usernameError", "Updtaing failed (" + e.getMessage() + ")");
		}
		// navigation
		if (error) {
			request.setAttribute("editedUser", newU);
			request.getRequestDispatcher(request.getContextPath() + "../WEB-INF/jsp/userAccount.jsp").forward(request, response);
		} else {
			request.getSession().setAttribute("user", newU);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

	}


	

package myproject.controllers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


import myproject.services.ImagesService;
import myproject.services.ProduitService;
import project.models.Produits;

@WebServlet("/products/new")
@MultipartConfig
public class AddProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProduitService ps = new ProduitService();
	private ImagesService is = new ImagesService();
	
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("../WEB-INF/jsp/addProduct.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Set<File> images = new HashSet<File>();
		if (request.getParameterValues("filesFromUrl") != null) {
			for (String fileUrl : request.getParameterValues("filesFromUrl")) {
				URL url = new URL(fileUrl);
				InputStream fileContent = url.openStream();
				images.add(is.saveImage(fileContent, request.getParameter("nom")));
			}
		}

		for (Part part : request.getParts()) {
			if ((part.getContentType() != null) && 
				part.getContentType().equals("image/jpeg")) {
				InputStream fileContent = part.getInputStream();
				images.add(is.saveImage(fileContent, request.getParameter("nom")));
			}
		}

		Produits p = new Produits(request.getParameter("nom"),
								Double.parseDouble(request.getParameter("prix")),
								request.getParameter("description"));
								
		p.setImages(images);
		ps.ajouterProduit(p);

		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
	}



			

	}




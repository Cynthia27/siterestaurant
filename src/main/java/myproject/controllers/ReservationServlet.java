package myproject.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myproject.services.ReservationService;
import project.models.Reservation;

@WebServlet("/reservation")
public class ReservationServlet  extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	private ReservationService rs = new ReservationService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/reservation.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

	Reservation r =	new Reservation(
				request.getParameter("heureDArrivee"),
				request.getParameter("nombreDePersonnes"),
				 LocalDate.parse(request.getParameter("dateArrivee"), DateTimeFormatter.ofPattern("MM/dd/yyyy") ),
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("numeroDeTelephone"));
		rs.addReservation(r);
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
	}
	
	
								
}
	


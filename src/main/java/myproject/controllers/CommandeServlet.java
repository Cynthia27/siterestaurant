package myproject.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import myproject.services.PersonneService;
import project.models.Commande;
import project.models.LigneProduit;
import project.models.Utilisateur;


@WebServlet("/user/commande")
public class CommandeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private PersonneService ps = new PersonneService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("../WEB-INF/jsp/commande.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur u = (Utilisateur)request.getSession().getAttribute("user");
		System.out.println(u);
		Commande c = new Commande(u,u.getPanier().getLigneProduits());
		u.getPanier().setListeProduits(new ArrayList<LigneProduit>());
		ps.addOrder(c);
		
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));			

	}


}

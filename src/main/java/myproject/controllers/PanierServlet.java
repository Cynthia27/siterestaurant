package myproject.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import myproject.services.ProduitService;
import myproject.services.PersonneService;
import project.models.Utilisateur;


@WebServlet("/user/panier")
public class PanierServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



PersonneService us = new PersonneService();
ProduitService ps = new ProduitService();


protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.getRequestDispatcher("../WEB-INF/jsp/panier.jsp").forward(request, response);
}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	int idProduit = Integer.parseInt(request.getParameter("product"));
	int nombre = Integer.parseInt(request.getParameter("quantity"));
	Utilisateur u = (Utilisateur) request.getSession().getAttribute("user");
	us.updatePanier(u, idProduit, nombre);
	response.setContentType("text/html); charset=utf-8");
	response.setStatus(HttpServletResponse.SC_OK);
	
}

}

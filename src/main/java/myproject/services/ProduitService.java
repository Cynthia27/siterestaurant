package myproject.services;



import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


import myproject.daos.ProduitDao;
import myproject.utils.HibernateUtil;
import project.models.Produits;
public class ProduitService {
	
	private ProduitDao produitDao = new ProduitDao();
	
		
	public void ajouterProduit(Produits p){
		produitDao.save(p);
	}
	
	public void saveProduct(Produits p) {
		produitDao.save(p);
	}
	
	public void updateProduct(Produits op, Produits np) {
		produitDao.save(np);
	}
	
	public void deleteProductById(int id) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			Produits p = produitDao.findById(id, s);
			if (p != null)
				produitDao.delete(p, s, t);
			t.commit();
		} catch(HibernateException e) {
			t.rollback();
			throw e;
		}
	}
	
	
	
	public List<Produits> filtrerParTexteEtPrix(String str, double min, double max) {
		return produitDao.findByNomOuDescriptionEtMinMax(str, min, max);
	}


		
}

	


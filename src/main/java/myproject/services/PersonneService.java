package myproject.services;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import myproject.daos.CommandeDao;
import myproject.daos.LigneProduitDao;
import myproject.daos.PanierDao;
import myproject.daos.ProduitDao;
import myproject.daos.UtilisateurDao;
import myproject.daos.PersonneDao;
import myproject.utils.HibernateUtil;
import project.models.Commande;
import project.models.LigneProduit;
import project.models.Personne;
import project.models.Produits;
import project.models.Utilisateur;

public class PersonneService {

	private PersonneDao personneDao = new PersonneDao();
	private UtilisateurDao utilisateurDao= new UtilisateurDao();
	private ProduitDao produitDao = new ProduitDao();
	private PanierDao panierDao = new PanierDao();
	private LigneProduitDao ligneProduitDao = new LigneProduitDao();
	private CommandeDao commandeDao = new CommandeDao();

	public PersonneService() {
		super();
		utilisateurDao= new UtilisateurDao();
	}



	public void ajouterUtilisateur(Utilisateur u) {	
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		utilisateurDao.save(u, s, t);
		t.commit();

	}

	public boolean usernameExists(String un) {
		return (personneDao.findByEmail(un) != null);
	}


	public Personne signIn(String un, String pwd) {
		return personneDao.findByEmailAndPassword(un, pwd);
	}

	public void updatePanier(Utilisateur u, int idProduit, int nb) {
		System.out.println("Updating panier");
		// ouverture session & transaction
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			
			LigneProduit lp = null;
			// recherche du produit (la ligne produit) dans le panier
			for (LigneProduit lp1 : u.getPanier().getListeProduits()) {
				if (lp1.getProduits().getId() == idProduit) {
					lp = lp1;
					break;
				}
			}
			if (lp == null) {
				if (nb > 0) {
					System.out.println("Nouvel lp");
					// on recupere le produit
					Produits p = produitDao.findById(idProduit, s);
					// creation de la ligne produit
					lp = new LigneProduit();
					lp.setNb(nb);
					lp.setProduits(p);
					// liaison de p a lp
					p.getLigneProduit().add(lp);
					// liaison de panier à lp
					u.getPanier().getListeProduits().add(lp);
					// sauvegarde
//					panierDao.save(c.getPanier(), s, t);
					ligneProduitDao.save(lp, s, t);
				}
			} else {
				if (nb != 0) {
					System.out.println("MaJ lp");
					// mise a jour de lp
					lp.setNb(nb);
					// sauvegarde
					ligneProduitDao.save(lp);
				} else {
					System.out.println("Suppression lp");
					// suppression de lp du panier
					System.out.println(u.getPanier().getListeProduits());
					u.getPanier().getListeProduits().remove(lp);
					System.out.println(u.getPanier().getListeProduits());
					// suppression de lp du produit
					System.out.println(lp.getProduits().getLigneProduit());
					lp.getProduits().getLigneProduit().remove(lp);
					System.out.println(lp.getProduits().getLigneProduit());
					lp.setProduits(null);
					panierDao.save(u.getPanier());
					ligneProduitDao.delete(lp);
				}
			}

			// commit
			t.commit();
		} catch(HibernateException e) {
			t.rollback();
			throw e;
		}
		// fermeture de la session
		s.close();
	}
	
	public void editerClient(Personne ancien, Utilisateur nouveau) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {	
			utilisateurDao.save(nouveau, s, t);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		}
	
	}
	
	public void addOrder(Commande c) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		commandeDao.save(c, s, t);
		t.commit();
		
}



	
	
}






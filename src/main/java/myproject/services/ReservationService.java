package myproject.services;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import myproject.daos.ReservationDao;
import myproject.utils.HibernateUtil;
import project.models.Reservation;

public class ReservationService {

	private ReservationDao reservationDao = new ReservationDao();


	public void addReservation(Reservation p){
		reservationDao.save(p);
	}
	public void saveReservation(Reservation p) {
		reservationDao.save(p);
	}

	public void updateReservation(Reservation op, Reservation np) {
		reservationDao.save(np);
	}

	public void deleteReservationById(int id) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			Reservation p = reservationDao.findById(id, s);
			if (p != null)
				reservationDao.delete(p, s, t);
			t.commit();
		} catch(HibernateException e) {
			t.rollback();
			throw e;
		}
	}
		
		
		public List<Reservation> filtrerParNom(String n) {
			return reservationDao.findReservationByName(n);
		
		
	}


}

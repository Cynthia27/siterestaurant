package myproject.daos;

import java.util.List;

import org.hibernate.Session;

import myproject.utils.HibernateUtil;
import project.models.Produits;


public class ProduitDao extends GenericDao<Produits> {

	public ProduitDao() {
		super(Produits.class);
		
	}
	
	
	public List<Produits> findByNomOuDescriptionEtMinMax(String str, double min, double max, Session s) {
		return s.createQuery("from Produits where (nom like :motif or description like :motif) and prix > :min and prix < :max")
				.setParameter("motif", "%"+str+"%")
				.setParameter("min", min)
				.setParameter("max", max)
				.list();
	}

	public List<Produits> findByNomOuDescriptionEtMinMax(String str, double min, double max) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Produits> lp = findByNomOuDescriptionEtMinMax(str, min, max, s);
		s.close();
		return lp;
	}
	
	
	
	
	
	
}


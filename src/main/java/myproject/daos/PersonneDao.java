
package myproject.daos;

import org.hibernate.Session;

import myproject.utils.HibernateUtil;
import project.models.Personne;


	
	public class PersonneDao extends GenericDao<Personne> {

		public PersonneDao() {
			super(Personne.class);
		}

		public Personne findByUsername(String email, Session s) {
			return (Personne) s.createQuery("from Utilisateur where email = :email")
					.setParameter("email", email)
					.uniqueResult();
		}

		public Personne findByEmail(String email) {
			Session s = HibernateUtil.getSessionFactory().openSession();
			Personne c = findByUsername(email, s);
			s.close();
			return c;
		}
		
		

		public Personne findByEmailAndPassword(String email, String password, Session s) {
			return (Personne) s.createQuery("from Utilisateur where email = :email and password = :password")
					.setParameter("email", email)
					.setParameter("password", password)
					.uniqueResult();
		}

		public Personne findByEmailAndPassword(String email, String password) {
			Session s = HibernateUtil.getSessionFactory().openSession();
			Personne c = findByEmailAndPassword(email, password, s);
			s.close();
			return c;
		}
		
		
	}


package myproject.daos;

import java.util.List;

import org.hibernate.Session;

import myproject.utils.HibernateUtil;
import project.models.Reservation;

public class ReservationDao extends GenericDao<Reservation> {

	public ReservationDao() {
		super(Reservation.class);	
	}
	
	public List<Reservation> findReservationByName (String n) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Reservation> r = findReservationByName(n, s);
		s.close();
		return r;	
	}
	
	public List<Reservation>findReservationByName(String n, Session s) {
		return s.createQuery("from Reservation where name= :name")
		   		.setParameter("name", n)
				.list();
	}
	
	
	public Reservation findReservationByDate (String n) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Reservation r = findReservationByDate(n, s);
		s.close();
		return r;	
	}
	
	public Reservation findReservationByDate(String n, Session s) {
		return (Reservation) s.createQuery("from Reservation where date= :date")
		   		.setParameter("date", n)
				.uniqueResult();
	}
	
	
	

}

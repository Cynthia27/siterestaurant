package myproject.daos;

import project.models.LigneProduit;

public class LigneProduitDao extends GenericDao<LigneProduit>{

	public LigneProduitDao() {
		super(LigneProduit.class);
		
	}

}

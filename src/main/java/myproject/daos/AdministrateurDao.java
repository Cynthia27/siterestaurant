package myproject.daos;

import project.models.Administrateur;

public class AdministrateurDao extends GenericDao<Administrateur>{

	public AdministrateurDao() {
		super(Administrateur.class);
		
	}

}

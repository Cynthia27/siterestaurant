package myproject.daos;

import org.hibernate.Session;

import myproject.utils.HibernateUtil;
import project.models.Commande;

public class CommandeDao extends GenericDao<Commande> {

	public CommandeDao() {
		super(Commande.class);
	}


	public Commande findOrderbyOrderNumber(String num) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Commande p = findOrderbyOrderNumber(num, s);
		s.close();
		return p;		
	}

	public Commande findOrderbyOrderNumber(String num, Session s) {
		return (Commande) s.createQuery("from Commande where numero= :numero")
		   		.setParameter("numero", num)
				.uniqueResult();
	}
	
	public Commande findOrderbyOrderUser(String user) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Commande p = findOrderbyOrderUser(user, s);
		s.close();
		return p;		
	}

	public Commande findOrderbyOrderUser(String user, Session s) {
		return (Commande) s.createQuery("from Commande where utilisateur= :utilisateur")
		   		.setParameter("utilisateur", user)
				.uniqueResult();
	}
	

}



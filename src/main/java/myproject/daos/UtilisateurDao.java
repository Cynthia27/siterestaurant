package myproject.daos;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import myproject.utils.HibernateUtil;
import project.models.Utilisateur;



	
	public class UtilisateurDao extends GenericDao<Utilisateur> {

		public UtilisateurDao() {
			super(Utilisateur.class);
		}

		
		
		public Utilisateur findByUsername(String email, Session s) {
			return (Utilisateur) s.createQuery("from Utilisateur where email = :email")
					.setParameter("email", email)
					.uniqueResult();
		}

		public Utilisateur findByUsername(String email) {
			Session s = HibernateUtil.getSessionFactory().openSession();
			Utilisateur c = findByUsername(email, s);
			s.close();
			return c;
		}
		

		public Utilisateur findByUsernameAndPassword(String email, String password, Session s) {

			return (Utilisateur) s.createCriteria(Utilisateur.class)
					.add(Restrictions.and(
							Restrictions.eq("email", email), 
							Restrictions.eq("password",  password)))
					.uniqueResult();
		}

		public Utilisateur findByUsernameAndPassword(String email, String password) {
			Session s = HibernateUtil.getSessionFactory().openSession();
			Utilisateur c = findByUsernameAndPassword(email, password, s);
			s.close();
			return c;
		}
		
		

}

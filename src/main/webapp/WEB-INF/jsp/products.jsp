<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Nino - La Carte</title>
<link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' />

<!-- base url for all links -->
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base
	href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />



<!-- JQuery -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Popper -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<!-- FontAwesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
	integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	crossorigin="anonymous">

<!-- Custom CSS & JS -->
<link rel="stylesheet" type="text/css" href="css/products.css">
<script src="js/product.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">



</head>
<body>

	<%@ include file="header.jsp"%>



	<div class="content">
		<div class="products">
			<c:forEach items="${products}" var="product">

				<div class="product" data-id="${product.id}">
					<div class="image">
						<span class="helper"></span>
						<c:if test="${ product.images.size() > 0 }">
							<img
								src="img/${product.images.iterator().next().getAbsolutePath().substring(product.images.iterator().next().getAbsolutePath().lastIndexOf('\\')+1)}"
								class="img-fluid">
						</c:if>
						<c:if test="${ product.images.size() == 0 }">
							<img src="img/burger.jpg" class="img-fluid">
						</c:if>
					</div>
					<div class="txt">
						<div class="nom" data-id="${product.id}">${product.nom}</div>
						<div class="description">${product.description}</div>
					</div>
					<div class="px">
						<div class="prix"  data-id="${product.id}">${product.prix}€</div>
						<c:if test="${!empty user }">
							<div class="ajoutQuantitePanier">
								<label class="quantite">Quantité</label><br> <input
									type="number" class="ajoutQuantitePanier" name="ajoutQuantitePanier" min="0" max="500000"
									value="${user.panier.getQuantite(product)}"
									data-id="${product.id}">
								<div class="fas fa-shopping-basket basket" data-toggle="modal" data-target="#myModal">								
								</div>
							</div>
						</c:if>
					</div>
				</div>
				
			</c:forEach>
		</div>
		 
  </div>
		
















	<!-- Footer ---------------------------------------------------------->




</body>
</html>
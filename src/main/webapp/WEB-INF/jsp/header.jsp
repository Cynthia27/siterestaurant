<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="js/header.js"></script>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/header.css">
<link href='http://fonts.googleapis.com/css?family=Lobster+Two'
	rel='stylesheet' type='text/css'>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="nav navbar-left">
			<li><a id="headerFont"
				href="https://www.instagram.com/azelie27/"
				class="fab fa-instagram fa-2x" data-toggle="tooltip"
				title="Instagram"></a></li>
			<li><a id="headerFont" href="https://www.tripadvisor.fr/"
				class="fab fa-tripadvisor fa-2x" data-toggle="tooltip"
				title="TripAdvisor"></a></li>
		</div>
		<div class="navbar-header">
			<a id="brandname" class="navbar-brand" href="">Nino</a>
		</div>
		<ul class="nav navbar-left">
			<li><a id="headerFont" href="reservation">Réserver</a></li>
			<li><a id="headerFont" href="contact">Contact</a></li>
			<li><a id="headerFont" href="products">Carte</a></li>
		</ul>

		<ul class="nav navbar-right">
			<c:if test="${empty user}">
				<li><a id="headerFont" href="user/signup">Sign Up</a></li>
			</c:if>
			<c:if test="${empty user}">

				<li><a id="headerFont" href="login">Login</a></li>
			</c:if>
			<c:if test="${!empty user}">
				<li><a id="headerFont" href="logout">Logout</a></li>
				<c:if test="${user.getRoles().contains('Utilisateur')}">
				<li><a id="headerFont" href="user/edit">Mon Compte</a></li></c:if>
				<c:if test="${user.getRoles().contains('Admin')}">
					<li><a id="headerFont" href="products/new">Add items</a></li>
					<!-- <li><a id="headerFont" href="products/new">Bookings</a></li>
					<li><a id="headerFont" href="products/new">Orders</a></li> -->
				</c:if>
			</c:if>
			<c:if test="${!empty user}">
				<li><a id="headerFont" href="user/panier"><span
						class="fas fa-shopping-basket"></span></a></li>
			</c:if>
		</ul>
	</div>
</nav>

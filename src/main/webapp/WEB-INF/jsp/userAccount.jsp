<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Nino - Mon Compte</title>
<link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' />

<!-- base url for all links -->
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base
	href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />



<!-- JQuery -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Popper -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<!-- FontAwesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
	integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	crossorigin="anonymous">

<!-- Custom CSS & JS -->
<link rel="stylesheet" type="text/css" href="css/signup.css">
<script src="js/userAccount.js"></script>
 <meta name="viewport" content="width=device-width, initial-scale=1">



</head>
<body>

	<%@ include file="header.jsp"%>
	<div class="content">
	<form action="user/edit" method="POST" class="form-group">
	
	<div class="content-form">
	<button id="profil" >Coordonnées</button>
	<div class="grid" id="edit">	
	<div class="nomsignup" >
					<input type="text" id="nomsignup"  class="form-control" name="nom"
							placeholder="Nom *" value="${editedUser.nom}" />
					<div style="color: red; display: none"></div></div>
					
					<div class="prenomsignup">
						<input type="text" class="form-control" name="prenom"
							placeholder="Prénom *" value="${editedUser.prenom}" />	
					<div style="color: red; display: none"></div></div>
				
					<div class="emailsignup">
						<input type="email" class="form-control" name="email"
							placeholder="Email  *" value="${editedUser.email}" />
					<div style="color: red; display: none"></div></div>
					
					<div class="telsignup">
						<input type="tel" class="form-control" name="numeroDeTelephone"
							placeholder="Téléphone *" value="${editedUser.numeroDeTelephone}" />
										<div style="color: red; display: none"></div></div>
											<div class="adressesignup">
				<input type="text" class="form-control" name="adresse"
					placeholder="Adresse *" value="${editedUser.adresse}" />
			<div style="color: red; display: none"></div></div>
										<!-- <div class="passwordsignup">
						<input type="password" class="form-control" name="oldPassword"
							placeholder="Ancien mot de passe *" value="" />
					<div style="color: red; display: none"></div></div>
				
				<div class="passwordsignup">
						<input type="password" class="form-control" name="newPassword"
							placeholder="Mot de passe *" value="" />
					<div style="color: red; display: none"></div></div>
					
					<div class="pwconfsignup">
						<input type="password" class="form-control" name="newPasswordConfirmation"
							placeholder="Confirmer *" value="" />
					<div style="color: red; display: none"></div></div>
				 -->
			
			<div class = "buttons">
			<input type="reset" class="btn btn-secondary" />
				 <input type="submit" id="submit" class="btn btn-success" />
				 </div>
				 </div>
	<%-- <button id="userorders">Mes commandes</button>
	<c:forEach items="${user.commandeUtilisateur}" var="commande">

<div class="orderRecap"data-id="${commande.id}">
${panier.commande.listeProd}
		</div>
			
			
</c:forEach></div> --%>
	
	<div class="content">
	
	
	</div>
	
	<button id="back"><a href="">Retour</a></button>
	
	

	</div>
	</form>
	</div>
	

</body>
</html>
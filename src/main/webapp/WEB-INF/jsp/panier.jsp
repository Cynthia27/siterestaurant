<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Nino - Mon Panier</title>
<link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' />

<!-- base url for all links -->
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base
	href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />



<!-- JQuery -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Popper -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<!-- FontAwesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
	integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	crossorigin="anonymous">

<!-- Custom CSS & JS -->
<link rel="stylesheet" type="text/css" href="css/panier.css">
<script src="js/product.js"></script>
 <meta name="viewport" content="width=device-width, initial-scale=1">



</head>
<body>

	<%@ include file="header.jsp"%>
	
<div class="content">
<c:if test="${!empty user.panier.ligneProduits}">
<h3 align="center">Votre panier</h3></c:if>
<div class="panier">

<c:if test="${empty user.panier.ligneProduits}">
<div id ="sadEmptybasket" class="sadEmptybasket"><p> Le panier est vide </p> </div>
</c:if>

<c:forEach items="${user.panier.ligneProduits}" var="ligneProduct">

		<div class="userPanier"data-id="${ligneProduct.produits.id}">
			<div class="productName" >${ligneProduct.produits.nom }</div>	
			<div class="prixProd">${ligneProduct.produits.prix}€</div>	
			<div class="ajoutQuantitePanier" >
				<label class="quantite" data-id="${ligneProduct.produits.id}">Quantité</label><br>
					<input type="number" class="ajoutQuantitePanier" name="ajoutQuantitePanier" data-id="${ligneProduct.produits.id}" data-prixU="${ligneProduct.produits.prix}" min="0" max="500000" value=${ligneProduct.nb }></div>	
					<div class="update">
					<div  class="fas fa-times-circle trash" data-id="${ligneProduct.produits.id}"><span class="tooltiptext">Supprimer</span></div> 
					
					</div>
																
		</div>	
		</c:forEach>
		
		<div class="prixTotal" >	
		<c:if test="${!empty user.panier.ligneProduits}"><p>Sous-total:<span class="total">${user.panier.prixTotal()} € </span>	</p>
		<button id="order"><a href="user/commande">Commander</a></button>
		</c:if>
		
		<button id="goToMenu"><a href="products">Retour à la carte</a></button>
		
		<c:if test="${!empty user.panier.ligneProduits}">
		<button id="reload" class="reload" >Reload </button>	
		
		</c:if></div>
		</div>
		</div>
		
		
		
		
</body>
</html>
	
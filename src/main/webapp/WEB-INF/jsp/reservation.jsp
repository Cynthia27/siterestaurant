<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>reservation</title>


<!-- JQuery -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Popper -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<link rel="stylesheet"
	href="https://formden.com/static/cdn/bootstrap-iso.css" />
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
<!-- FontAwesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
	integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	crossorigin="anonymous">
	
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="i18n/datepicker-fr.js"></script>
<!-- Custom CSS & JS -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/reservation.css">
<script src="js/reservation.js"></script>
 


<script	var ctx = "${pageContext.request.contextPath}">
</script>
</head>
<body>

	<%@ include file="header.jsp"%>
	
	<form action="reservation" method="POST" class="form-group" id="resaInput">
		<div class="content">
			<div class="resa">
				<div id="nombreDePersonnes" class="nombreDePersonnes">
					<label class="control-label requiredField" for="nombreDePersonnes">Combien
						de personnes? 
					</label> <select required id="nb-personnes-select" class="form-control select-mini"
						name="nombreDePersonnes"  >
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
					</select>
				</div>
				
				<div class="datepickr" id="dropEffect1">
				 <label class="control-label requiredField" for="date"> Date:
					</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-calendar"> </i>
						</div>
						 <input name="dateArrivee" type="text" id="datepicker" autocomplete="off">
					</div> 
				</div>	
				<div class="timepickr" id="dropEffect2" >
				<label class="control-label requiredField" for="heure"> Heure:
					</label> <br>
					<select id="time" name="heureDArrivee" class="form-control">
					<option value=""></option>
						<option value="18:00">18:00</option>
						<option value="19:00">19:00</option>
						<option value="20:00">20:00</option>
					</select>
				</div>
				<div class="nom" id="dropEffect3" >
				<c:if test="${!empty user }">
				<label class="nom" for="nom" >Nom: 		
					</label> <br>
					<input required type="text" name="nom" class="form-control" value="${user.nom}" /></c:if>
					<c:if test="${empty user }">
				<label class="nom" for="nom" >Nom: 		
					</label> <br>
					<input required type="text" name="nom" class="form-control" /></c:if>
					<div style="color: red; display: none"></div>
				</div> 
				<div class="prenom" id="dropEffect4">
				<c:if test="${!empty user }">
				<label class="firstname">Prénom</label>
				<input required type="text" name="prenom" class="form-control"value="${user.prenom}" /></c:if>
				<c:if test="${empty user }">
				<label class="firstname">Prénom</label>
				<input required type="text" name="prenom" class="form-control"/></c:if>
				<div style="color: red; display: none"></div>
				</div> 
				<div class="tel" id="dropEffect5">
				<c:if test="${!empty user }">
					<label class="control-label requiredField" for="telephone">Téléphone
							</label> 
					<input required type="tel" name="telephone" class="form-control" value="${user.numeroDeTelephone}"/></c:if>
						<c:if test="${empty user }">
					<label class="control-label requiredField" for="telephone">Téléphone
							</label> 
					<input required type="tel" name="telephone" class="form-control" /></c:if>
					<div style="color: red; display: none"></div>
				</div> 	
				
			</div>	
			<div class="text-center" id="dropEffect6">
			<button id="reset"><a href="reservation">Reset</a></button>
					<input type="submit" id="submit" class="btn btn-success"<a href="reservation/validation"></a> />
				</div>	
				</div>	
		</form>
		
</body>
</html>
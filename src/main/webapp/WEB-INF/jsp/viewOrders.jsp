<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Orders</title>
	<link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' />
	<!-- base url for all links -->
	<c:set var="url">${pageContext.request.requestURL}</c:set>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

	<!-- JQuery -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	<!-- Custom CSS & JS -->
	
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script src="js/images-selector.js"></script>
	
	
</head>
<body>

<%@ include file="header.jsp"%>

<div class="content">
		
		<div class="commande">
		<fieldset class="test">
	<div class ="gridProd">
<c:forEach items="${user.panier.ligneProduits}" var="ligneProduct">
<ul><li>
<div class="productRecap"data-id="${ligneProduct.produits.id}">

${ligneProduct.produits.nom }
		${ligneProduct.produits.prix}€
		</div></li></ul>
			
			
</c:forEach></div>
</fieldset>

<div class="adresses">
<div class ="adresseAct" ><input id="current" type="radio" name="adress" value="current" checked>Adresse actuelle:<br> ${user.adresse} </div>
<div class ="adresseSwitch" ><input id="adresseChecked"  type="radio" name="adress" value="otherA">Autre adresse
<textarea id="other" name="autreAdresse" form="adressFord"></textarea></div></div>
<div class="mdp">
<input id="cb" type="radio" name="pp" value="cb" checked><i class="fab fa-cc-visa fa-2x"></i>
<input id="paypal"  type="radio" name="pp" value="pp"><i class="fab fa-cc-paypal fa-2x"></i></div>
<fieldset class="test">Prix total
<div class="recapCommande">${user.panier.prixTotal()} € </div></fieldset>

<div class = "buttons">
			<button id="back"><a href="user/panier">Retour</a></button>
				 <input type="submit" id="submit" class="btn btn-success" />
				 </div>
			
				 
 </div>
 	

		
	</div>

</body>
</html>
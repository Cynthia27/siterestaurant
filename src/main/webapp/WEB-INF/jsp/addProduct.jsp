<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title> New Product</title>
	<link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon' />
	<!-- base url for all links -->
	<c:set var="url">${pageContext.request.requestURL}</c:set>
    <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

	<!-- JQuery -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	<!-- Custom CSS & JS -->
	
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	<script src="js/images-selector.js"></script>
	<link rel="stylesheet" type="text/css" href="css/addprod.css">
	
	
</head>
<body>

<%@ include file="header.jsp"%>

<div class="content">
			<form action="products/new" method="POST" enctype="multipart/form-data">
				<fieldset>
					<h3 class= "titre" align="center" >New product</h3>
					<label for="nom">Nom:</label>
					<input type="text" name="nom" class="form-control" />
					<label for="description">Description:</label>
					<textarea name="description" class="form-control"></textarea> 
					<label for="prix">Prix:</label>
					<input type="text" name="prix" class="form-control">
					<label for="images">Images:</label>
					
					 <div class="border rounded" style="width:100%;display:grid;grid-template-columns:1fr 1fr;">
						<div class="text-center border-right border-bottom p-1">File from URL</div>
						<div class="text-center border-bottom p-1">File from Disk</div>
						<div class="border-right p-1" style="height:100px;">
							<ul id="file-from-url-container" class="list-unstyled m-1">
							</ul>
						</div>
						<div class="p-1" style="height:100px;">
							<ul id="file-from-disk-container" class="list-unstyled m-1">
							</ul>
						</div>
						<div style="display:grid; grid-template-columns:1fr auto auto;" class="p-1 border-right">
							<input id="file-from-url-input" type="text" name="fileUrl" class="form-control ml-1">
							<button class="btn btn-success ml-2" type="button" disabled>
								<span class="fas fa-plus fa-lg"></span>
							</button>
							<button class="btn btn-danger ml-1" type="button" disabled>
								<span class="fas fa-times fa-lg" ></span>
							</button>
						</div>
						<div style="display:grid; grid-template-columns:1fr auto auto;" class="p-1">
							<input id="file-from-disk-input" type="file" name="fileDisk" class="form-control-file ml-1">
							<button class="btn btn-success ml-1" type="button" disabled>
								<span class="fas fa-plus fa-lg"></span>
							</button>
							<button class="btn btn-danger ml-1" type="button" disabled>
								<span class="fas fa-times fa-lg" ></span>
							</button>
						</div>
					</div> 
				</fieldset>
		
				<div class="text-center">
					<input type="reset" class="btn btn-secondary" />
					<input type="submit" id="submit" class="btn btn-success" />
				</div>
		
			</form>
		</div>

</body>
</html>
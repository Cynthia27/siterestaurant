

$(document).ready(function(){

	var error;
	var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	function invalid(e, m) {
		e.css("border", "2px solid red");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}

	function valid(e) {
		e.next().hide();
		e.css("border", "2px none red");
	}
	
	$("input[name=email]").keyup(function() {
		
		var req = new XMLHttpRequest();
		req.open("GET", "utilisateur/exists?email="+$(this).val(), true);
		req.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	if (this.responseText == "true") {
		    		invalid($("input[name=email]"), "Cet email est déjà utilisé.");
		    	} else {
		    		valid($("input[name=email]"));
		    	}
		    }
		};
		req.send();
	});

	$(".content form ").submit(function() {
		error = false;


		if ($("input[name=email]").val() == "") {
			invalid($("input[name=email]"), "Champ obligatoire.");
		}

		else if (!emailRegExp.test($("input[name=email]").val())) {
			invalid($("input[name=email]"), "Veuillez rentrer une adresse email valide.");
		} else {
			valid($("input[name=email]"));
		}

		if ($("input[name=adresse]").val() == "") {
			invalid($("input[name=adresse]"), "Champ obligatoire.");
		} else {
			valid($("input[name=adresse]"));
		}

		
	
		if ($("input[name=prenom]").val() == "") {
			invalid($("input[name=prenom]"), "Champ obligatoire.");
		} else {
			valid($("input[name=prenom]"));
		}

		if ($("input[name=nom]").val() == "") {
			invalid($("input[name=nom]"), "Champ obligatoire.");
		} else {
			valid($("input[name=nom]"));
		}
		
		if ($("input[name=numeroDeTelephone]").val() == "") {
			invalid($("input[name=numeroDeTelephone]"), "Champ obligatoire.");
		} else {
			valid($("input[name=numeroDeTelephone]"));
		}

	
		if ($("input[name=password]").val() == "") {
			invalid($("input[name=password]"), "Champ obligatoire.");
		}

		else if ($("input[name=password]").val().length > 8) {
			invalid($("input[name=password]"), "Le mot de passe ne doit pas dépasser 8 caractères.");
		}

		else if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) { // 
			invalid($("input[name=password]"));
		} else {
			valid($("input[name=password]"));
		}

	
		if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) {
			invalid($("input[name=passwordConfirmation]"), "The confirmation does not match the password.");
		} else {
			valid($("input[name=passwordConfirmation]"));
		}
		
		

		return !error;
	});

});

$(document).ready(function(){

	function updatePanier(productId, oQ, nQ,done) {
		if (done==undefined)
			done= function(){};
			$.ajax({
				method: "POST",
				url: "user/panier",
				data: {product: productId,quantity: nQ }
			})
			.done(done());
	}


	function newUpdate() {
		if (nQ > 0) {
			console.log("updating ligne");
			// updating quantity
			$(".ajoutQuantitePanier input[data-id="+productId+"]").val(nQ);
			$(".ajoutQuantitePanier input[data-id="+productId+"]").attr(nQ);
			// updating ligneProduit
			let pU = Number($(".prix[data-id="+productId+"]").text());
			$(".prixTotal[data-id="+productId+"]").text(nQ*pU);
			// updating panier total
			let t = Number($(".prixTotal").text());
			$(".prixTotal").text(t - (oQ*pU) + (nQ*pU))
		} else {
			console.log("removin ligne");
			// updating panier total
			let pU = Number($(".prix[data-id="+productId+"]").text());
			let t = Number($(".prixTotal").text());
			$(".prixTotal").text(t - (oQ*pU))
			// removing ligneProduit
			$(".products[data-id="+productId+"]").remove();
		}

	};
	
	


	$(document).ready(function(){
		$(".basket").click(function(){
			let inputVal = $(this).prev().val();
			let id = $(this).prev().attr("data-id");
			console.log(inputVal, id)
			updatePanier(id, 0, inputVal, newUpdate);

		});




		$("#reload").click(function() {
			var total=0;
			$("input.ajoutQuantitePanier").each(function(){
				let newVal = Number($(this).val());
				let id = Number($(this).attr("data-id"));
				let pu = Number($(this).attr("data-prixU"));
				updatePanier(id, 0, newVal);
				console.log(newVal, total);
				total += newVal*pu;    	
			});

			$(".total").html(total+"€");

		});


		$(".trash").click(function() {
			let id = $(this).attr("data-id");
			let oQ = Number($(".quantite input[data-id="+id+"]").val());
			let nQ = 0;
			location.reload();
			updatePanier(id, oQ, nQ);


		});  		
	});	
});
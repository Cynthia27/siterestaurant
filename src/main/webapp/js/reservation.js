

$(document).ready(function(){

	$(function() {
		$( "#datepicker" ).datepicker({ 
			minDate: "0D", maxDate: "+1M +10D", 
			format: 'dd/mm/yyyy',
		});
	});
	
	  $( function() { 
		    function runEffect() {
		      var drop;
		      $( "#dropEffect1" ).show( drop, 500, callback );
		    };    
		    function callback() {
		        setTimeout(function() {
		          $( "#dropEffect1:visible" ).removeAttr( "style" ).fadeOut();
		        }, 1000 );
		      };   
		      $( "#nb-personnes-select" ).change(function() {
		        runEffect();
		      });   
		      $( "#dropEffect1" ).hide();
		    } ); 
	  
	  $( function() { 
		    function runEffect() {
		      var drop;
		      $( "#dropEffect2" ).show( drop, 500, callback );
		    };    
		    function callback() {
		        setTimeout(function() {
		          $( "#dropEffect2:visible" ).removeAttr( "style" ).fadeOut();
		        }, 1000 );
		      };   
		      $( "#datepicker" ).change(function() {
		        runEffect();
		      });   
		      $( "#dropEffect2" ).hide();
		    } );  
	
	  $( function() { 
		    function runEffect() {
		      var drop;
		      $( "#dropEffect3" ).show( drop, 500, callback );
		    };    
		    function callback() {
		        setTimeout(function() {
		          $( "#dropEffect3:visible" ).removeAttr( "style" ).fadeOut();
		        }, 1000 );
		      };   
		      $( "#time" ).change(function() {
		        runEffect();
		      });   
		      $( "#dropEffect3" ).hide();
		    } );  
	  
	  $( function() { 
		    function runEffect() {
		      var drop;
		      $( "#dropEffect4" ).show( drop, 500, callback );
		    };    
		    function callback() {
		        setTimeout(function() {
		          $( "#dropEffect4:visible" ).removeAttr( "style" ).fadeOut();
		        }, 1000 );
		      };   
		      $( "#time" ).change(function() {
		        runEffect();
		      });   
		      $( "#dropEffect4" ).hide();
		    }); 
	  
	  $( function() { 
		    function runEffect() {
		      var drop;
		      $( "#dropEffect5" ).show( drop, 500, callback );
		    };    
		    function callback() {
		        setTimeout(function() {
		          $( "#dropEffect5:visible" ).removeAttr( "style" ).fadeOut();
		        }, 1000 );
		      };   
		      $( "#time" ).change(function() {
		        runEffect();
		      });   
		      $( "#dropEffect5" ).hide();
		    } ); 
	  
	  $( function() { 
		    function runEffect() {
		      var drop;
		      $( "#dropEffect6" ).show( drop, 500, callback );
		    };    
		    function callback() {
		        setTimeout(function() {
		          $( "#dropEffect6:visible" ).removeAttr( "style" ).fadeOut();
		        }, 1000 );
		      };   
		      $( "#time" ).change(function() {
		        runEffect();
		      });   
		      $( "#dropEffect6" ).hide();
		    } ); 
	  
	  function invalid(e, m) {
			e.css("border", "2px solid red");
			if (m !== undefined) {
				e.next().text(m);
				e.next().show();
			} else {
				e.next().hide();
			}
			error = true;
		}

		function valid(e) {
			e.next().hide();
			e.css("border", "2px none red");
		}
	  
	  
	  $(".content form ").submit(function() {
			error = false;

		if ($("input[name=prenom]").val() == "") {
			invalid($("input[name=prenom]"), "Champ obligatoire.");
		} else {
			valid($("input[name=prenom]"));
		}

		if ($("input[name=nom]").val() == "") {
			invalid($("input[name=nom]"), "Champ obligatoire.");
		} else {
			valid($("input[name=nom]"));
		}
		
		if ($("input[name=telephone]").val() == "") {
			invalid($("input[name=telephone]"), "Champ obligatoire.");
		} else {
			valid($("input[name=telephone]"));
		}

		return !error;
		});
	  
	 
	 
});









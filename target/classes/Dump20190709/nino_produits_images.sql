-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: nino
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `produits_images`
--

DROP TABLE IF EXISTS `produits_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `produits_images` (
  `Produits_id` int(11) NOT NULL,
  `images` tinyblob,
  KEY `FKqbpnaskdmff6ik0lp7xyp8mco` (`Produits_id`),
  CONSTRAINT `FKqbpnaskdmff6ik0lp7xyp8mco` FOREIGN KEY (`Produits_id`) REFERENCES `produits` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produits_images`
--

LOCK TABLES `produits_images` WRITE;
/*!40000 ALTER TABLE `produits_images` DISABLE KEYS */;
INSERT INTO `produits_images` VALUES (5,_binary '�\�\0sr\0java.io.File-�E\r\��\0L\0patht\0Ljava/lang/String;xpt\03C:\\Users\\Admin\\Documents\\RestaurantPics\\Pizza-0.jpgw\0\\x'),(6,_binary '�\�\0sr\0java.io.File-�E\r\��\0L\0patht\0Ljava/lang/String;xpt\04C:\\Users\\Admin\\Documents\\RestaurantPics\\Burger-0.jpgw\0\\x'),(7,_binary '�\�\0sr\0java.io.File-�E\r\��\0L\0patht\0Ljava/lang/String;xpt\08C:\\Users\\Admin\\Documents\\RestaurantPics\\Lemon cake-0.jpgw\0\\x');
/*!40000 ALTER TABLE `produits_images` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-09 12:08:58
